﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace townNameGen_V2
{
    class Program
    {
        public static List<String> readIn(string fileName)
        {
            List<String> holder = new List<String>();
            using (StreamReader reader = new StreamReader(fileName)) // Have reader read the file, dispose of it when finished
            {
                string line; //Hold current line

                while ((line = reader.ReadLine()) != null) // Read the line, and if not null
                {
                    holder.Add(line); //Add the line to the List
                }

                return holder; //return this so we can set the lists equal to it

            }
        }

        static void Main(string[] args)
        {
            Console.Title = "Town Name Generator"; //Set title

            Random rand = new Random();

            List<String> prefix = readIn("prefix.txt");//Stores Prefixs
            List<String> name = readIn("name.txt"); //Stores Body of name
            List<String> suffix = readIn("suffix.txt"); //Stores Suffixs

            for (int i = 0; i < 24; i++) //We'll be outputting 24 of these, so we put it in a for loop
            {

                string pre = ""; //Declare and Initialize
                string mid = ""; //Declare and Initialize
                string suf = ""; //Declare and Initialize

                if (rand.Next(100) < 33) //If the random number is less than 33, we will add a prefix  
                    pre = prefix[rand.Next(prefix.Count)] + " "; //Sets the prefix if applicable by grabbing a random number from the range of the list, and going to that index
                mid = name[rand.Next(name.Count)]; // Sets mid part by grabbing a random number from the range of the list, and going to that index
                suf = suffix[rand.Next(suffix.Count)]; // Sets suffix by grabbing a random number from the range of the list, and going to that index

                Console.WriteLine(pre + mid + suf); //Outputs the name
                using (StreamWriter writer = new StreamWriter("log.txt", true)) //Makes a new streamwriter to write to log.txt, and append it
                {
                    writer.WriteLine(pre + mid + suf); //Adds the name
                }
            }
            Console.WriteLine("Press Any Key to Exit"); //To let the user know if they press something it will close
            Console.ReadKey(); //Waits for a keypress to close
        }
    }
}
